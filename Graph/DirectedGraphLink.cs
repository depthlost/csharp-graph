
namespace GraphStructure
{
    public class DirectedGraphLink<N, L> : GraphLink<N, L>
    {
        private IGraphNode<N, L> node;
        
        public DirectedGraphLink(IGraphNode<N, L> node, L data) : base(data)
        {
            this.node = node;
        }

        public override IGraphNode<N, L> GetNode(IGraphNode<N, L> sourceNode) => node;
    }
}
