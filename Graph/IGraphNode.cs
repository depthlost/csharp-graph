
namespace GraphStructure
{
    public interface IGraphNode<N, L>
    {
        public N data { get; set; }

        public void AddLink(IGraphLink<N, L> link);
        public IGraphLink<N, L> RemoveLink(IGraphLink<N, L> link);
        public void Link(IGraphNode<N, L> node, L linkData);
        public IGraphLink<N, L> Unlink(IGraphNode<N, L> node);
        public IGraphLink<N, L> GetLink(IGraphNode<N, L> node);
        public IEnumerable<IGraphLink<N, L>> GetLinks();
        public IEnumerable<IGraphLink<N, L>> GetLinks(Func<L, bool> predicate);
        public L GetLinkData(IGraphNode<N, L> node);
        public void UpdateLinkData(IGraphNode<N, L> node, L linkData);
        public bool IsAdjacent(IGraphNode<N, L> node);
    }
}
