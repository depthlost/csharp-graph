

namespace GraphStructure
{
    public class Graph<I, N, L> : IGraph<I, N, L> where I: notnull
    {
        private Dictionary<I, IGraphNode<N, L>> nodes;
        private bool isDirected;

        public Graph(bool isDirected)
        {
            nodes = new Dictionary<I, IGraphNode<N, L>>();
            this.isDirected = isDirected;
        }

        public bool AddNode(I nodeID, N nodeData)
        {
            return nodes.TryAdd(nodeID, new GraphNode<N, L>(this, nodeData));
        }

        public IGraphLink<N, L> GetLink(I sourceNodeID, I destinationNodeID)
        {
            return GetNode(sourceNodeID).GetLink(GetNode(destinationNodeID));
        }

        public L GetLinkData(I sourceNodeID, I destinationNodeID)
        {
            return GetLink(sourceNodeID, destinationNodeID).data;
        }

        public IGraphNode<N, L> GetNode(I nodeID)
        {
            return nodes[nodeID];
        }

        public N GetNodeData(I nodeID)
        {
            return GetNode(nodeID).data;
        }

        public I GetNodeID(IGraphNode<N, L> node)
        {
            return nodes.First(each => each.Value == node).Key;
        }

        public bool IsAdjacent(I sourceNodeID, I destinationNodeID)
        {
            return GetNode(sourceNodeID).IsAdjacent(GetNode(destinationNodeID));
        }

        public bool IsEmpty()
        {
            return nodes.Count == 0;
        }

        public void LinkNode(I sourceNodeID, I destinationNodeID, L linkData)
        {
            GetNode(sourceNodeID).Link(GetNode(destinationNodeID), linkData);
        }

        public void LinkNode(IGraphNode<N, L> sourceNode, IGraphNode<N, L> destinationNode, L linkData)
        {
            if (isDirected)
            {
                sourceNode.AddLink(new DirectedGraphLink<N, L>(destinationNode, linkData));
            }
            else
            {
                IGraphLink<N, L> link = new UndirectedGraphLink<N, L>(sourceNode, destinationNode, linkData);
            
                sourceNode.AddLink(link);
                destinationNode.AddLink(link);
            }
        }

        public IGraphNode<N, L>? RemoveNode(I nodeID)
        {
            nodes.Remove(nodeID, out IGraphNode<N, L>? value);
            return value;
        }

        public List<IGraphNode<N, L>> SearchInBreadth(I nodeID)
        {
            return SearchInBreadth(nodeID, linkData => true);
        }

        public List<IGraphNode<N, L>> SearchInBreadth(I nodeID, Func<L, bool> predicate)
        {
            List<IGraphNode<N, L>> nodePath = new List<IGraphNode<N, L>>();
            HashSet<IGraphNode<N, L>> visitedNodes = new HashSet<IGraphNode<N, L>>();
            Queue<IGraphNode<N, L>> nextNodes = new Queue<IGraphNode<N, L>>();
            IGraphNode<N, L> currentNode;
            IGraphNode<N, L> nextNode;

            nextNode = GetNode(nodeID);
            nextNodes.Enqueue(nextNode);
            visitedNodes.Add(nextNode);

            while (nextNodes.Count > 0)
            {
                currentNode = nextNodes.Dequeue();
                nodePath.Add(currentNode);

                foreach (IGraphLink<N, L> link in currentNode.GetLinks(predicate))
                {
                    nextNode = link.GetNode(currentNode);
                    
                    if (!visitedNodes.Contains(nextNode))
                    {
                        nextNodes.Enqueue(nextNode);
                        visitedNodes.Add(nextNode);
                    }
                }
            }

            return nodePath;
        }

        public List<IGraphNode<N, L>> SearchInDepth(I nodeID)
        {
            return SearchInDepth(nodeID, linkData => true);
        }

        private void SearchInDepth(IGraphNode<N, L> node, Func<L, bool> predicate, HashSet<IGraphNode<N, L>> visitedNodes, List<IGraphNode<N, L>> nodePath)
        {
            IGraphNode<N, L> nextNode;

            nodePath.Add(node);
            visitedNodes.Add(node);

            foreach (IGraphLink<N, L> link in node.GetLinks(predicate))
            {
                nextNode = link.GetNode(node);
                
                if (!visitedNodes.Contains(nextNode))
                    SearchInDepth(nextNode, predicate, visitedNodes, nodePath);
            }
        }

        public List<IGraphNode<N, L>> SearchInDepth(I nodeID, Func<L, bool> predicate)
        {
            HashSet<IGraphNode<N, L>> visitedNodes = new HashSet<IGraphNode<N, L>>();
            List<IGraphNode<N, L>> nodePath = new List<IGraphNode<N, L>>();

            SearchInDepth(GetNode(nodeID), predicate, visitedNodes, nodePath);

            return nodePath;
        }

        public IGraphLink<N, L> UnlinkNode(I sourceNodeID, I destinationNodeID)
        {
            return GetNode(sourceNodeID).Unlink(GetNode(destinationNodeID));
        }

        public IGraphLink<N, L> UnlinkNode(IGraphNode<N, L> sourceNode, IGraphNode<N, L> destinationNode)
        {
            IGraphLink<N, L> link = sourceNode.GetLink(destinationNode);

            sourceNode.RemoveLink(link);

            if (!isDirected)
                destinationNode.RemoveLink(link);

            return link;
        }

        public void UpdateLinkData(I sourceNodeID, I destinationNodeID, L linkData)
        {
            GetNode(sourceNodeID).UpdateLinkData(GetNode(destinationNodeID), linkData);
        }

        public void UpdateNodeData(I nodeID, N nodeData)
        {
            GetNode(nodeID).data = nodeData;
        }
    }
}
