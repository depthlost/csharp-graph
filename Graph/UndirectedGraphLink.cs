
namespace GraphStructure
{
    public class UndirectedGraphLink<N, L> : GraphLink<N, L>
    {
        private IGraphNode<N, L> leftNode;
        private IGraphNode<N, L> rightNode;
        
        public UndirectedGraphLink(IGraphNode<N, L> leftNode, IGraphNode<N, L> rightNode, L data) : base(data)
        {
            this.leftNode = leftNode;
            this.rightNode = rightNode;
        }

        public override IGraphNode<N, L> GetNode(IGraphNode<N, L> sourceNode)
        {
            return sourceNode == rightNode ? leftNode : rightNode;
        }
    }
}
