
namespace GraphStructure
{
    public abstract class GraphLink<N, L> : IGraphLink<N, L>
    {
        public L data { get; set; }

        public GraphLink(L data)
        {
            this.data = data;
        }

        public abstract IGraphNode<N, L> GetNode(IGraphNode<N, L> sourceNode);
    }
}
