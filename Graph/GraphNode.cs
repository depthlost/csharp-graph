

namespace GraphStructure
{
    public class GraphNode<N, L> : IGraphNode<N, L>
    {
        private IBaseGraph<N, L> graph;
        public N data { get; set; }
        public List<IGraphLink<N, L>> links { get; private set; }

        public GraphNode(IBaseGraph<N, L> graph, N data)
        {
            this.graph = graph;
            this.data = data;
            links = new List<IGraphLink<N, L>>();
        }

        public void AddLink(IGraphLink<N, L> link)
        {
            links.Add(link);
        }

        public IGraphLink<N, L> RemoveLink(IGraphLink<N, L> link)
        {
            links.Remove(link);
            return link;
        }

        public void Link(IGraphNode<N, L> node, L linkData)
        {
            graph.LinkNode(this, node, linkData);
        }

        public IGraphLink<N, L> Unlink(IGraphNode<N, L> node)
        {
            return graph.UnlinkNode(this, node);
        }

        public IGraphLink<N, L> GetLink(IGraphNode<N, L> node)
        {
            return links.First(each => each.GetNode(this) == node);
        }

        public L GetLinkData(IGraphNode<N, L> node)
        {
            return GetLink(node).data;
        }

        public void UpdateLinkData(IGraphNode<N, L> node, L linkData)
        {
            GetLink(node).data = linkData;
        }

        public bool IsAdjacent(IGraphNode<N, L> node)
        {
            return links.Any(each => each.GetNode(this) == node);
        }

        public IEnumerable<IGraphLink<N, L>> GetLinks()
        {
            return links;
        }

        public IEnumerable<IGraphLink<N, L>> GetLinks(Func<L, bool> predicate)
        {
            return links.Where(each => predicate(each.data));
        }
    }
}
