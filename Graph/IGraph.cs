
namespace GraphStructure
{
    public interface IBaseGraph<N, L>
    {
        public bool IsEmpty();
        public void LinkNode(IGraphNode<N, L> sourceNode, IGraphNode<N, L> destinationNode, L linkData);
        public IGraphLink<N, L> UnlinkNode(IGraphNode<N, L> sourceNode, IGraphNode<N, L> destinationNode);
    }
    
    public interface IGraph<I, N, L> : IBaseGraph<N, L> where I: notnull
    {
        public bool IsAdjacent(I sourceNodeID, I destinationNodeID);

        public IGraphNode<N, L> GetNode(I nodeID);
        public I GetNodeID(IGraphNode<N, L> node);
        public N GetNodeData(I nodeID);

        public bool AddNode(I nodeID, N nodeData);
        public void UpdateNodeData(I nodeID, N nodeData);
        public IGraphNode<N, L>? RemoveNode(I nodeID);

        public IGraphLink<N, L> GetLink(I sourceNodeID, I destinationNodeID);
        public void LinkNode(I sourceNodeID, I destinationNodeID, L linkData);
        public void UpdateLinkData(I sourceNodeID, I destinationNodeID, L linkData);
        public IGraphLink<N, L> UnlinkNode(I sourceNodeID, I destinationNodeID);
        public L GetLinkData(I sourceNodeID, I destinationNodeID);

        public List<IGraphNode<N, L>> SearchInDepth(I nodeID);
        public List<IGraphNode<N, L>> SearchInDepth(I nodeID, Func<L, bool> predicate);

        public List<IGraphNode<N, L>> SearchInBreadth(I nodeID);
        public List<IGraphNode<N, L>> SearchInBreadth(I nodeID, Func<L, bool> predicate);
    }
}
