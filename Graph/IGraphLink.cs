
namespace GraphStructure
{
    public interface IGraphLink<N, L>
    {
        public L data { get; set; }
        
        public IGraphNode<N, L> GetNode(IGraphNode<N, L> sourceNode);
    }
}
